title: Contaminazioni Python Rust
event: TCS Night 23 Gennaio 2018
contact: Matteo Bertini <naufraghi@develer.com> @naufraghi
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}
]

---

class: impact

# {{title}}
## {{event}} | {{contact}}

---

class: middle, center

# Chi sono io?

---

class: center

Ricordo vagamente questo momento

![Python nel 2000](python-nel-2000.png)

???

- Sviluppo in Python da un bel po' di anni, _ricordo vagamente il passaggio dalla versione 1.5 alla 1.6_

---

# Chi sono io?

Lavoro per Develer con Python, Cython, Qt, C++, Go e _speriamo_, Rust

![Develer](develer-site.png)

???

- Sono team leader in Develer in un progetto Python / Cython / C++ di ~ 200kloc
- Python è il mio linguaggio preferito per fare le cose velocemente, ma Rust si sta ritagliando
  un suo spazio quando le priorità sono:
  1. correttezza
  2. performance

---

# Di cosa parleremo oggi?

- i linguaggi si evolvono
- si influenzano tra loro
- le buone idee si diffondono

--

> Chi riceve un'idea da me, ricava conoscenza senza diminuire la mia; come chi accende la sua candela
> con la mia riceve luce senza lasciarmi al buio.

_Thomas Jefferson_

???

I linguaggi di programmazione si cambiano e si evolvono, è da programmatore
è bello vedere che le buone idee, o banalmente le idee a cui siamo abituati
trovano posto in più di un linguaggio.

---

# Contaminazioni*

> *semplificando irrigorosamente

--

- docstring e doctest: Python .medium[⇒] Rust

--

- protocolli / trait / magic methods: Python .medium[⇒] Rust

--

- compilatore come amico: Elm .medium[⇒] Rust .medium[⇒] Python (con Mypy?)

--

- gestione dei pacchetti: Ruby? .medium[⇒] Rust .medium[⇒] Python

???

- docstring e doctest, o _come essere sicuri che la documentazione sia aggiornata_
- protocolli / trait / magic methods, o _come scrivere un linguaggio senza l'overload degli operatori_
- compilatore come amico, o _come soffrire meno ad ogni release_
- gestione dei pacchetti, o _avere più di un progetto sotto mano senza dare di matto_

---

# Docstring e doctest in Python

```python
class Point2D:
    """
    Simple 2D point class

    >>> p1 = Point2D(1.0, 0.0)
    >>> p2 = Point2D(0.0, 1.0)
    >>> p1 + p2
    Point2D(1.0, 1.0)
    """
```

--

- [asciinema](https://asciinema.org/a/x2EMkMsrMYjXZaQCJ9XUhpEN8?theme=solarized-light&size=big)


???

- È possibile eseguire i doctest anche in file di sola documentazione, non
    solo `.py`

---

# Docstring e doctest in Rust

```rust
/// Point2D is a simple 2D point structure
///
/// ```rust
/// use geo::Point2D;
///
/// let p1 = Point2D::new(1.0, 0.0);
/// let p2 = Point2D::new(0.0, 1.0);
/// assert_eq!(format!("{:?}", p1 + p2), "Point2D { x: 1.0, y: 1.0 }");
/// ```_
pub struct Point2D {

}
```

--

- [asciinema](https://asciinema.org/a/jTST8B6xxjxCZXpVgcJa7D0Rt?theme=solarized-light&size=big)

???

- Come in Python il vantaggio di questo approccio non è tanto mirato a sostituire i test ma più
  che altro a garantire che la documentazione contenga esempi sempre aggiornati.
- IMHO un doctest che fa un po' da documentazione ed un po' da test è
  un ottimo strumento quando si è in fase di prototipaggio di una interfaccia.

---

class: middle

# Python special methods

```python
class Point2D:
    ...
    def __add__(self, other):
        return Point2D(self.x + other.x,
                       self.y + other.y)
```

???

Un linguaggio che usa i protocolli dalla libreria standard in su aiuta a strutturare
il codice nello stesso modo (niente di nuovo, anche Go usa pervasivamente le interfaccie)

---

class: middle

# Rust Traits

```rust
impl Add for Point2D {
    type Output = Point2D;
    fn add(self, other: Point2D) -> Point2D {
        Point2D {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
```

???

In Rust è stata fatta una scelta simile, però l'_aderenza_ ad un certo
protocollo (Trait) è dichiarata e controllata a compile time.

---

# Errori in Python?

```python
def mean_point(p1, p2):
    return (p1 + p2) / 2
```

Più tardi ...

```terminal
     12
     13 def mean_point(p1, p2):
---> 14     return (p1 + p2) / 2

TypeError: unsupported operand type(s) for /: 'Point2D' and 'int'
```
???

- Ovviamente il codice non da errore fino a quando non lo si esegue
- Il messaggio di errore è quello che è, non da molti suggerimenti su come risolvere il problema

---

# Errori in Rust?

```rust
fn mean_point(p1: Point2D, p2: Point2D) -> Point2D {
    (p1 + p2) / 2.0
}
```

Presto ...

```terminal
error[E0369]: binary operation `/` cannot be applied to type `Point2D`
  --> src/lib.rs:39:5
   |
39 |     (p1 + p2) / 2.0
   |     ^^^^^^^^^^^^^^^
   |
   = note: an implementation of `std::ops::Div` might be missing for `Point2D`
```

???

- Ovviamente il codice non compila
- Il messaggio di errore da anche una indicazione su come risolvere il problema.

Invece Rust ha risentito come tanti altri linguaggi dell'influsso di Elm ed il
compilatore contiene un estratto della documentazione e cerca di dare sempre un
buon indizio per risolvere il problema.


---

# Mypy
> checker statico per Python, basato sulle annotazioni di tipo

```python
def mean_point(p1: Point2D, p2: Point2D) -> Point2D:
    return (p1 + p2) / 2
```

--

L'errore viene trovato!

```terminal
$ mypy --strict mean_point.py

mean_point.py:16: error: Unsupported operand types for / ("Point2D" and "int")
```

???

- ma sulla amichevolezza degli errori c'è un po' da lavorare

---

# Cargo + Rustup

.small[
```terminal
Rust's package manager
Usage:
    cargo <command> [<args>...]
    cargo [options]
Options:
    -h, --help          Display this message
    ...
Some common cargo commands are (see all commands with --list):
    build       Compile the current project
    check       Analyze the current project and report errors, but don't
                build object files
    clean       Remove the target directory
    doc         Build this project's and its dependencies' documentation
    new         Create a new cargo project
    ...
See 'cargo help <command>' for more information on a specific command.
```
]

--

- demo `cargo doc` o [Cargo Doc](cargo-doc.png)

???

```terminal
$ cd ~/Documents/src/many-python-rust/circle-rs
$ cargo doc --open
```

---

# Pipenv + Pyenv?

.small[
```terminal
Usage: pipenv [OPTIONS] COMMAND [ARGS]...
Options:
  --update         Update Pipenv & pip to latest.
  --where          Output project home information.
  ...
Usage Examples:
   Create a new project using Python 3.6, specifically:
   $ pipenv --python 3.6
   ...
Commands:
  check      Checks for security vulnerabilities and against PEP 508 markers
             provided in Pipfile.
  graph      Displays currently–installed dependency graph information.
  install    Installs provided packages and adds them to Pipfile, or (if none
             is given), installs all packages.
  ...
```
]

???

- con Pipfile e Pipfile.lock anche Python è arrivato nel secondo millennio del
    package management!
- sulla documentazione c'è ancora un po' da lavorare.

---

# Integrazione

- `rust-cpython` wrapper API CPython (Rust stabile)
  [source](https://github.com/dgrunwald/rust-cpython/blob/master/extensions/tests/custom_class.rs)
- `PyO3` fork unstable con maggiore ergonomicità
  [source](https://pyo3.github.io/pyo3/guide/class.html)
- `Milkshake` cbindgen + cffi (*Pypy friendly*)
  [source](https://github.com/getsentry/milksnake/blob/master/example/rust/src/lib.rs)
- `Cython` da esplorare [source](https://github.com/mckaymatt/rust_pypi_example/blob/master/rust_pypi_example/rust/src/lib.rs),
  [source](https://gitlab.com/naufraghi/rust_pypi_example/blob/master/rust_pypi_example/rust/src/lib.rs)

---

# rust-cpython

.micro[
```rust
#![crate_type = "dylib"]

#[macro_use] extern crate cpython;

use cpython::{PyObject, PyResult};

py_module_initializer!(custom_class, initcustom_class, PyInit_custom_class, |py, m| {
    try!(m.add(py, "__doc__", "Module documentation string"));
    try!(m.add_class::<MyType>(py));
    Ok(())
});

py_class!(class MyType |py| {
    data data: i32;
    def __new__(_cls, arg: i32) -> PyResult<MyType> {
        MyType::create_instance(py, arg)
    }
    def a(&self) -> PyResult<PyObject> {
        println!("a() was called with self={:?}", self.data(py));
        Ok(py.None())
    }
});
```
]

---

# PyO3

.small[
```rust
#[py::method]
impl MyClass {

     #[new]
     fn __new__(obj: &PyRawObject, ...) -> PyResult<()> {
         obj.init(|token| {
             MyClass {
                 num: 10,
                 debug: False,
                 token: token
             }
         })
     }
}
```
]

---

# Milkshake

.small[
```rust
#[repr(C)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

#[no_mangle]
pub unsafe extern "C" fn example_get_origin() -> Point {
    Point { x: 0.0, y: 0.0 }
}
```
]

--

.small[
```python
from . import _native

def test():
    point = _native.lib.example_get_origin()
return (point.x, point.y)
```
]

---

# Cython

.small[
```python
cdef extern from "rust_pypi_example.h":
    cpdef int is_prime(const int n)

    ctypedef void* counter
    cdef counter counter_new()
    cdef void counter_free(counter)
    cdef void counter_start(counter)
    cdef int counter_current(counter)

cdef class Counter:
    cdef counter __counter
    def __cinit__(self):
        self.__counter = counter_new()
    def __dealloc__(self):
        counter_free(self.__counter)
    def start(self):
        counter_start(self.__counter)
    def current(self):
        return counter_current(self.__counter)
```
]

---

# Quale scegliere?

--

.col-6[
## rust-cpython / PyO3

- le interazioni con Rust sono infrequenti
- accettiamo di compilare la libreria Rust una volta per versione / piattaforma
- non ci interessa Pypy
]

--

.col-6[
## Milkshake / Cython
- Python e Rust interagiscono in un loop stretto
- vogliamo compilare la libreria Rust meno volte possibile
- accettiamo di scrivere una api C verbosa
- che però potrebbe essere utile anche per altre integrazioni
]

---

# Bonus track

Algoritmo genetico, con l'obiettivo di avvicinare tra se N cerchi su un piano.

--

- 92 linee di Python 3 (dipendenze escluse: solo `attrib`)
- 211 linee di Rust (dipendenze escluse: `rand` e `rayon`)

---

## Circles demo

- <u onclick="Elm.Main.embed(document.getElementById('svg-demo'))">Start demo</u>, `r` to reverse/restart

--

- per i curiosi, genero codice Elm

<div id="svg-demo" width="100%" height="100%" style="margin-top: -250px"></div>

---

# Porting

- abbastanza facile

--

- spesso sono passato da loop imperativi a catene di iteratori

--

- mi manca tantissimo `itertools`

--

- mi mancano i generatori (però ci sono in unstable)

---

# Performance v0

.col-6[
- CPython v0: undefined
- Pypy v0: 2 min
]

--

.col-6[
- Rust v0: 2 min .alt[.big[!!??]]
- Rust v0 parallel: 4 min .alt[.big[!!!!????]]
]

---

## Profiling

- in Python avevo un generatore (`itertools.combinations`) ed un early exit

--

- alcuni metodi venivano chiamati più volte, anche se i dati "sotto" erano immutabili

--

- `operf` faceva emergere l'allocazione di memoria e `combinations2` come maggiori punti caldi del codice Rust

--

- parallelizzare non faceva altro che peggiorare la situazione delle cache

---

## Parallelizzare in Rust

.micro[
```diff
+use rayon::prelude::*;
 
 
 #[derive(Clone, Copy, Debug)]
@@ -201,7 +201,7 @@ impl Evolution {
         }
     }
     fn selection(&self, mut growing_population: Vec<Placement>) -> Self {
-        growing_population.sort_unstable_by(|p1, p2| {
+        growing_population.par_sort_unstable_by(|p1, p2| {
             p1.fitness()
                 .partial_cmp(&p2.fitness())
                 .expect("Unexpected nan")
@@ -218,7 +218,7 @@ impl Evolution {
         let couples = rand::seq::sample_slice(&mut rng, &self.population, size * 2);
         couples
             .as_slice()
-            .windows(2)
+            .par_windows(2)
             .map(|s| s[0].crossover(&s[1], mutation_offset))
             .collect()
     }
```
]

---

# Performance v1

Calcolo gli attributi al momento della creazione della classe.

--

.col-6[
- CPython v1: undefined
- Pypy v1: 52 sec
]

--

.col-6[
- Rust v1: 5 sec
- Rust v0 parallel: 2 sec
]

---

## Take away

- i linguaggi di alto livello sono meno sensibili al codice scritto "male"

--

- ma comunque sono sensibili: _v1_ va più di 2 volte più veloce

--

- i linguaggi di basso livello non hanno molta libertà di "interpretazione"

--

- ma quando scrivi codice "migliore", il guadagno e evidente: _v1_ va più di 40 volte più veloce

--

- e finalmente, parallelizzare ha senso

---

# Reference

- [Python data model](https://docs.python.org/3/reference/datamodel.html)
- [Elm Compiler error for humans](http://elm-lang.org/blog/compiler-errors-for-humans)
- [Doctest - Test interactive Python examples](https://docs.python.org/3/library/doctest.html)
- [Rust Documentation](https://doc.rust-lang.org/book/first-edition/documentation.html)
- [Mypy - Optional Static Typing for Python](http://mypy-lang.org/)
- [This repo and slides](https://gitlab.com/naufraghi/ga-nesting-demo)
