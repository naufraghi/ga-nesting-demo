extern crate rand;
extern crate rayon;

use rayon::prelude::*;

mod point;
mod circle;

pub use point::Point2D;
pub use circle::Circle;

fn combinations2<T: Clone>(pool: &Vec<T>) -> Vec<(&T, &T)> {
    let r = 2;
    let mut res = vec![];
    let n = pool.len();
    if r > n {
        return res;
    }
    let mut indices: Vec<_> = (0..r).collect();
    let reversed: Vec<_> = (0..r).rev().collect();
    res.push((&pool[indices[0]], &pool[indices[1]]));
    loop {
        let idx = (|| {
            for i in reversed.iter() {
                if indices[*i] != (*i + n - r) {
                    return Some(*i);
                }
            }
            None
        })();
        if let Some(i) = idx {
            indices[i] += 1;
            for j in (i + 1)..r {
                indices[j] = indices[j - 1] + 1;
            }
            res.push((&pool[indices[0]], &pool[indices[1]]));
        } else {
            break;
        }
    }
    res
}

#[derive(Clone, Debug)]
pub struct Placement<T> {
    pub shapes: Vec<T>,
    _valid: bool,
    _dispersion: f32,
}

enum PlacementState {
    Valid(f32),
    Invalid(f32),
}


pub trait Shape {
    fn overlap(&self, other: &Self) -> f32;
    fn distance(&self, point: Point2D) -> f32;
    fn offset(&self, offset: Point2D) -> Self;
    fn mutation(&self, offset: f32) -> Self;
}

impl<T> Placement<T> where T: Shape + Sized + Clone {
    pub fn new(shapes: Vec<T>) -> Self {
        let (mutual_distance, valid) = match Placement::mutual_distance(&shapes) {
            PlacementState::Valid(score) => (score, true),
            PlacementState::Invalid(score) => (score * 10., false),
        };
        let origin_distance = Placement::origin_distance(&shapes) * 0.1;
        let dispersion = mutual_distance + origin_distance;
        Placement {
            shapes: shapes,
            _valid: valid,
            _dispersion: dispersion,
        }
    }
    pub fn valid(&self) -> bool {
        self._valid
    }
    fn mutual_distance(shapes: &Vec<T>) -> PlacementState {
        let (score, valid) = combinations2(shapes)
            .iter()
            .map(|&(s1, s2)| s1.overlap(&s2))
            .fold((0., true), |(tot, valid), d| {
                (tot + if d > 0. { d } else { 100. + 100. * -d }, valid && (d > 0.))
            });
        if valid {
            PlacementState::Valid(score)
        } else {
            PlacementState::Invalid(score)
        }
    }
    fn origin_distance(shapes: &Vec<T>) -> f32 {
        let b = Point2D::origin();
        shapes.iter().map(|ref c| c.distance(b)).sum()
    }
    fn dispersion(&self) -> f32 {
        self._dispersion
    }
    pub fn fitness(&self) -> f32 {
        self.dispersion()
    }
    pub fn shuffled(&self) -> Self {
        Placement::new(self.shapes.iter().map(|ref s| s.mutation(1.0)).collect())
    }
    pub fn crossover(&self, other: &Self, mutation_offset: f32) -> Self {
        use rand::Rng;
        let mut rng = rand::thread_rng();
        let split = rng.gen_range(1, self.shapes.len());
        let shapes = self.shapes
            .iter()
            .take(split)
            .chain(other.shapes.iter().skip(split));
        Placement::new(shapes.map(|ref s| s.mutation(mutation_offset)).collect())
    }
}


#[derive(Debug)]
pub struct Evolution<T> {
    population: Vec<Placement<T>>,
    size: usize,
    generation: usize,
}

impl<T> Evolution<T> where T: Shape + Sized + Clone + Sync + Send {
    pub fn create(item: &Placement<T>, size: usize) -> Evolution<T> {
        Evolution {
            population: (0..size).map(|_| item.clone().shuffled()).collect(),
            size: size,
            generation: 0,
        }
    }
    fn selection(&self, mut growing_population: Vec<Placement<T>>) -> Self {
        growing_population.par_sort_unstable_by(|p1, p2| {
            p1.fitness()
                .partial_cmp(&p2.fitness())
                .expect("Unexpected nan")
        });
        Evolution {
            population: growing_population.into_iter().take(self.size).collect(),
            size: self.size,
            generation: self.generation + 1,
        }
    }
    fn crossover(&self, mutation_offset: f32) -> Vec<Placement<T>> {
        let mut rng = rand::thread_rng();
        let size = self.size / 4;
        let couples = rand::seq::sample_slice(&mut rng, &self.population, size * 2);
        couples
            .as_slice()
            .par_windows(2)
            .map(|s| s[0].crossover(&s[1], mutation_offset))
            .collect()
    }
    pub fn evolve(&self, mutation_offset: f32) -> Self {
        //! Perform an evolution step
        self.selection(
            self.population
                .clone()
                .into_iter()
                .chain(self.crossover(mutation_offset))
                .collect(),
        )
    }
    pub fn best(&self) -> &Placement<T> {
        &self.population[0]
    }
}
