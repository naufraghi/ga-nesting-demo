from collections import namedtuple


class Point2D(namedtuple("Point2D", "x y")):
    """
    Simple 2D point class

    >>> p1 = Point2D(1.0, 0.0)
    >>> p2 = Point2D(0.0, 1.0)
    >>> p1 + p2
    Point2D(x=1.0, y=1.0)
    """
    def __add__(self, other):
        return Point2D(self.x + other.x,
                       self.y + other.y)
