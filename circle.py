#!/usr/bin/env python3

"""
Python prototype of circle packing
"""
from random import randint, uniform, sample
from math import sqrt, cos, sin
from itertools import combinations, chain
from collections import namedtuple

import attr


class Point2D(namedtuple("Point2D", ("x", "y"))):
    """
    Simple 2D point class

    >>> p1 = Point2D(1.0, 0.0)
    >>> p2 = Point2D(0.0, 1.0)
    >>> p1 + p2
    Point2D(x=1.0, y=1.0)
    """
    __slots__ = ()
    def __abs__(self):
        return sqrt(self.x**2 + self.y**2)
    def __add__(self, other):
        return Point2D(self.x + other.x, self.y + other.y)
    def __sub__(self, other):
        return Point2D(self.x - other.x, self.y - other.y)
    def __mul__(self, scalar):
        return Point2D(self.x * scalar, self.y * scalar)
    def __truediv__(self, scalar):
        return Point2D(self.x / scalar, self.y / scalar)
    @classmethod
    def origin(cls):
        return cls(0.0, 0.0)
    @classmethod
    def radial(cls, radius, angle):
        return cls(radius * cos(angle), radius * sin(angle))


class Circle(namedtuple("Circle", ("center", "radius"))):
    __slots__ = ()
    def overlap(self, other):
        """
        Return the length of the overlap:
        - (a & b) > 0 if there is no overlap
        - (a & b) < 0 if the distance of the centers is less than the sum of the radii
        """
        return abs(self.center - other.center) - (self.radius + other.radius)
    def offset(self, offset):
        return Circle(self.center + offset, self.radius)
    # interface for Evolution
    def mutation(self, offset=1.0):
        x = uniform(-offset, offset)
        y = uniform(-offset, offset)
        return self.offset(Point2D(x, y))


class Placement(namedtuple("Placement", ("shapes", "valid_", "dispersion_"))):
    __slots__ = ()
    def __new__(cls, shapes):
        mutual_distance, valid = cls._mutual_distance(shapes)
        if not valid:
            mutual_distance *= 10
        origin_distance = cls._origin_distance(shapes) * 0.1
        dispersion = mutual_distance + origin_distance
        return super(Placement, cls).__new__(cls, shapes, valid, dispersion)
    def valid(self):
        return self.valid_
    @staticmethod
    def _mutual_distance(shapes):
        """
        The best distance is the one where all the shapes are barely touching each other,
        but this score alone does not favor a "gravitational" placement.
        """
        valid = True
        def overlaps():
            nonlocal valid
            for s1, s2 in combinations(shapes, 2):
                o = s1.overlap(s2)
                if o < 0:
                    valid = False
                yield o
        return sum((d if d > 0 else 100 + -d * 100) for d in overlaps()), valid
    @staticmethod
    def _origin_distance(shapes):
        """
        This distance has a minimum in an invalid placement
        """
        b = Point2D.origin()
        return sum(abs(b - c.center) for c in shapes)
    def _dispersion(self):
        """
        Some measure of the placement dispersion, we prefer a low mutual distance near the origin.
        """
        return self.dispersion_
    # interface for Evolution
    def fitness(self):
        return self._dispersion()
    def shuffled(self):
        new_shapes = [s.mutation() for s in self.shapes]
        return Placement(new_shapes)
    def crossover(self, other, mutation_offset=1.0):
        split = randint(1, len(self.shapes) - 1)
        merged = chain(self.shapes[:split], other.shapes[split:])
        new_shapes = [s.mutation(offset=mutation_offset) for s in merged]
        return Placement(new_shapes)


# We do a brute force search using a genetic algorithm, the genome is the ordered list of the
# shapes:
# - the selection will reward a low dispersion
# - the crossover will operate on the list
# - the mutation will alter the position of the center of the shapes

@attr.s
class Evolution:
    population = attr.ib()
    size = attr.ib()
    generation = attr.ib()
    @classmethod
    def create(cls, item, size):
        items = [item.shuffled() for _ in range(size)]
        return cls(items, size, 0)
    def selection(self, growing_population):
        new_population = sorted(growing_population, key=lambda p: p.fitness())[:self.size]
        return Evolution(new_population, self.size, self.generation + 1)
    def crossover(self, mutation_offset=1.0):
        size = int(self.size / 4)
        couples = sample(self.population, size * 2)
        for s1, s2 in zip(couples[:-1], couples[1:]):
            yield s1.crossover(s2, mutation_offset=mutation_offset)
    def evolve(self, mutation_offset=1.0):
        growing_population = self.population + list(self.crossover(mutation_offset=mutation_offset))
        return self.selection(growing_population)
