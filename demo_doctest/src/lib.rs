/// Point2D is a simple 2D point structure
///
/// ```rust
/// use demo_doctest::Point2D;
///
/// let p1 = Point2D::new(1.0, 0.0);
/// let p2 = Point2D::new(0.0, 1.0);
/// assert_eq!(format!("{:?}", p1 + p2), "Point2D { x: 1.0, y: 1.0 }");
/// ```

use std::ops::Add;

#[derive(Debug)]
pub struct Point2D {
    x: f32,
    y: f32,
}


impl Point2D {
    pub fn new(x: f32, y: f32) -> Point2D {
        Point2D { x: x, y: y }
    }
}


impl Add for Point2D {
    type Output = Point2D;
    fn add(self, other: Point2D) -> Point2D {
        Point2D::new(self.x + other.x, self.y + other.y)
    }
}
