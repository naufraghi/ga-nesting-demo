from random import shuffle
from math import pi
from itertools import chain

import circle


def circles(sizes):
    radii = list(chain(*(([radius] * number) for (radius, number) in sizes)))
    shuffle(radii)
    num = len(radii)
    for i, radius in enumerate(radii):
        c = circle.Point2D.radial(num, 2*pi*i/num)
        yield circle.Circle(c, radius)


shapes = list(circles(((1, 20), (2, 10), (3, 4), (5, 2))))
shuffle(shapes)
p = circle.Placement(shapes)
e = circle.Evolution.create(p, 100)


def evolve(e, r):
    for _ in range(200):
        e = e.evolve(r)
    return e


elm_template = """\
module Main exposing (main)

import GraphicSVG exposing (..)

type Msg
    = Tick Float GetKeyState


type alias Circle =
    {{ radius : Float, x : Float, y : Float }}

type alias Placement =
    List Circle

type alias Model =
    {{ placements : List Placement, pos : Float, direction : Int }}


placements =
    [
    {placements}
    ]


init =
    {{ placements = placements, pos = 0, direction = 1 }}

update msg model =
    let
      cap pos =
        if pos < 0 then
           0
        else
           if pos >= List.length model.placements then
                (List.length model.placements) - 1
           else
                pos
    in
    case msg of
        Tick _ ( keys, _, _ ) ->
            case keys (Key "r") of
                JustDown ->
                    {{ model
                      | pos = cap (model.pos - model.direction)
                      , direction = -model.direction }}

                _ ->
                    {{ model | pos = cap (model.pos + model.direction) }}

blue_circle c =
    circle c.radius |> filled blue |> move (c.x, c.y) 

view model =
    let
        placement = List.drop model.pos model.placements |> List.head
    in
    collage {width} {height} 
        (case placement of
           Just p ->
              List.map blue_circle p
           _ ->
              []
        )

main =
    gameApp Tick {{ 
                   model = init
                 , update = update
                 , view = view 
                 }}
"""

circle_template = "{{ radius = {c.radius}, x = {c.center.x}, y = {c.center.y} }}"


placements = []

def info(e):
    print("fitness", e.population[0].fitness())
    p = e.population[0]
    print("valid", p.valid())
    s1, s2, s3 = p.shapes[:3]
    print("overlap 1-2", s1.overlap(s2))
    print("overlap 2-3", s2.overlap(s3))
    print("overlap 1-3", s1.overlap(s3))
    circles = ",\n  ".join([circle_template.format(c=c) for c in p.shapes])
    placements.append("[{}]".format(circles))
    with open("Circles.elm", "w+") as f:
        f.write(elm_template.format(width=100, height=100, placements="\n    ,\n    ".join(placements)))
    return s1, s2, s3


for i in range(100):
    e = evolve(e, 5.0/(i+1))
    info(e)
