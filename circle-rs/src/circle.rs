use rand;
use point::Point2D;

use super::Shape;

#[derive(Clone, Debug)]
pub struct Circle {
    pub center: Point2D,
    pub radius: f32,
}

impl Shape for Circle {
    fn overlap(&self, other: &Circle) -> f32 {
        //! Return the length of the overlap:
        //!
        //! - (a & b) > 0 if there is no overlap
        //! - (a & b) < 0 if the distance of the centers is less than the sum of the radii
        (self.center - other.center).abs() - (self.radius + other.radius)
    }
    fn distance(&self, point: Point2D) -> f32 {
        (self.center - point).abs()
    }
    fn offset(&self, offset: Point2D) -> Circle {
        Circle {
            center: self.center + offset,
            radius: self.radius,
        }
    }
    fn mutation(&self, offset: f32) -> Circle {
        use rand::Rng;
        let mut rng = rand::thread_rng();
        let x = rng.gen_range(-offset, offset);
        let y = rng.gen_range(-offset, offset);
        self.offset(Point2D::new(x, y))
    }
}
