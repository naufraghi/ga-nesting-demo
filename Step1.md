# Many ways to call Rust from Python

## Step 1

Mi serve un task computazionale, oppure con molto IO, per far vedere come è facile farlo in Rust.

Un task computazionale potrebbe essere:

> dato un insieme di cerchi (centro e raggio), calcolare la posizione dei centri in modo che la
> somma delle distanze dal baricentro sia minima.

Senza perdere di generalità possiamo assumere che il centro del cerchio di raggio maggiore sia
posizionato sull'origine.